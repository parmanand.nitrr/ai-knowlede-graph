formulas(assumptions).

%1.All objects x that belong to the Tomato Class, is a part of the food item class.

all x (Tomato(x) -> FoodItem(x)).

%2. All items x belonging to the class Tomato also belong to the class Veggie
all x (Tomato(x) -> Veggie(x)).

%3.All object x that belong to the Veggie Class, is a part of the Item class.
all x (Veggie(x) -> Item(x)).

%4.All object x that belong to the Beef Class, is a part of the Meat class.
all x (Beef(x) -> Meat(x)).

%5.All object x that belong to the Meat Class, is a part of the food item class.
all x (Meat (x) -> FoodItem(x)).

%6.All objects that belong to the Deodorant Class, belong to the PersonalCare class.
 all x (Deodorant (x) -> PersonalCare (x)).

%7. All objects that belong to the PersonalCare Class, belong to the Item class.
all x (PersonalCare (x) -> Item (x)).

%8. All people who eat meat are non-vegetarians.
all x all y (Eats(x,y) & FoodItem(y) & Meat(y) -> -Vegetarian(x)).

%9. John is a person.
Person(John).

%10. If supermarket owns something then they sell it.
all x all y all q (SuperMarket(x) & Item(y) & Owns(x,y,q) & Quantity(q) -> Sells(x,y,q)).

%11. Entity x has an object of item class y and the and the amount owned is represented
%by q which is an object of class Quantity.
Has(x,y,q).

%12.If someone buys an object of item class, then they own it.
all x all y all q  all z (Buys(x,y,q,z) & Item(y) -> Owns(x,y,q)).

%13. If someone buys something and it is a food item, then they eat it. 
all x all y all q all z(Buys(x,y,q,z) & FoodItem(y) -> Eats(x,y)).

%14. If x buys y and x has money m then after the transaction x will have less money than before. 

all x all y all q (Buys(x,y,q,z) & HasMoney(x,m1) & Money(m1) & SuperMarket(z) -> HasMoney(x,m2) &
Money(m2) & LessThan(m2,m1)).

%15. The location of Safeway is in NorthBerkeley as per the question statement.
Location(Safeway, NorthBerkeley).

%16.Safeway is a supermarket
SuperMarket(Safeway).

%17. Every supermarket buys some quantity of some deodrant from some manufacturer
all x all  y all q all z SuperMarket(x) & Quantity(q) &  Deodorant(y) & Buys(x,y,q,z) & Manufacturer(z).

%18.if x has money u then x can buy items from supermarket.
all x all y all z all q all u HasMoney(x,u) & Money(u) ->  Buys(x,y,q,z) & Item(y) & SuperMarket(z) .

%19 Every item has price tag.
  all x all y Item(x) -> Price(x,y).

%20 Every item  has weight.
all x all y Item(x) -> Weight(x,y).

%21 Every x whose age is greater than a1 is an adult.

all x all y all z Age(x,z) & Person(x) & Lessthan(a1,z) & Number(a1) -> Adult(x).

%22 Every x whose age is less  than a1 is a child.

all x all y all z Age(x,z) & Person(x) & Lessthan(z,a1) & Number(a1) -> Child(x).

%23 Every x who is a person has age.

all x all y Person(x)-> Age(x,y).

%24 All Supermarket buys all items from any manufacturer .

all x all y all q all z Manufacturer(z) -> Buys(x,y,q,z) & Supermarket(x) & Item(y) & Quantity(q).

%25 Typical 1 pound of tomatoes contains 4 tomatoes.
Tomato(x) & Quantity(q) & Pound(q1)=1 & HasCount(x,c1)=4.

%26 Money can be in credit.

%all x Money(x) & Money(credit) | Money(cash).

%27 1 pound = 16 oz
Pound(q1) = 1 & Ounces(o1)=16.

%28 Every supermarket has staff


%29 Staff are at supermarket and work


%30 Yesterday john was at North Berkeley safeway Supermarket 
At(John,Safeway) & Location(Safeway, NorthBerkeley).


%31 Yesterday John buys 2 pounds of tomatoes from Supermarket 
all z Buys(John,y2,q2,z) & Tomato(y2)& Quantity(q2) & SuperMarket(z) & Pound(q2)=2 & HasVolume(y2,v2).

%32 Yesterday John buys 1 pound of ground beef from Supermarket 

all z Buys(John,ground_beef,q1,z) & Beef(ground_beef)& Quantity(q1) & SuperMarket(z) & Pound(q1)=1 & HasVolume(y1,v1).

%33 Mary is a person 
Person(Mary).

%33 Yesterday Mary was at North Berkeley safeway Supermarket
At(Mary,Safeway) & Location(Safeway, NorthBerkeley).

%34 Yesterday Mary buys y pound of tomato
all z Buys(Mary,y2,q,z) & Tomato(y2)& Quantity(q) & SuperMarket(z).

%35 John's age is  greater than the age  set for adult.
%all y Age(John,y) & Lessthan(y,a1) & Number(a1) .

%37 All staff are person.
all x Staff(x) -> Person(x).

%38 Every supermarket has gas station next to it.
all x all y Next(x,y) & SuperMarket(x) & GasStation(y).

%39 Shell station is next to safeway located at NorthBerkley
Next(Safeway,Shell) & SuperMarket(Safeway) & GasStation(Shell).

%40 Gas station generally have gas.

all x all y GasStation(x) & HasGas(x,y).

%41 John owns a car of camry model.
HasCar(John,Camry) & Car(Camry).


%42 Every car has trunck.
all x Car(x) & HasTrunk(x,y) & Trunk(y). 

%43 Trunck has x  volume.

all x all y Trunk(x) & HasVolume(x,y) & Volume(y).

%44 Every item has volume.
all x all y Item(x) &  HasVolume(x,y) & Volume(y).

%45 if item has a voulme less that available volume in Trunk than item can be fit inside Trunk.
Item(x) & Trunk(y) & HasVolume(x,v1) & HasVolume(y,v2) & LessThan(v1,v2) -> fit(x,y). 


%46 All person fill gas  from gas station.

%47 if x buys gas  for its car from Y gas station then x has more gas than previosuly he had

%48 if x buys gas for  its car from Y gas station then gas station has less gas than previosuly it had
 
%49 when x and y are at same section  and same time, they x sees y and y sees x.

At(x,y) & At(z,y) & SuperMarket(y) & Item(p) & Buys(x,p,q,y) & Buys(z,p,q,y) -> Sees(x,z).

%50 Any x can buy from supermarket y only if it is open

%51 Tomato has Volume v1.


%52 Every container has volume

%53 When item x is placed inside container,container has less available volume.

%54 All items of same class are placed in same section.

SectionOf(x,z) & SectionOf(x,p) -> z=p. 

%55 Supermarket has  sections for each item.

Supermarket(x)-> HasSection(x,t) & SectionOf(p,t) & Item(t) & Section(p) . 

%56 All Person who  buys from supermarket are customers

%57 All Person who work at Organization are staff.

all x all y Person(x) & Work(x,y) & Organization (y) & At(x,y) -> Staff(x).

%58 All Supemarket are Organization. 
all x (Supermarket(x) -> Organization(x)).

%59 John has a car

%60 All container are empty initially

%61 if  x is a customer  then x buys quanitity q of item y  from supermarket.
all x all y all z all q all u Customer(x) & Buys(x,y,q,z) & Item(y) & SuperMarket(z) & HasMoney(x,u).

%62 if someone is not staff at supermarket  then he is customer supermarket.
%all x all y At(x,y) & SuperMarket(y) & -Staff(x) & Customer(x).

%63 All item has property count .
%all Item(x) -> HasCount(x,c).

%64 Any x is manufacturer then x makes y
all x all y Manufacturer(x) & Make(x,y) & Item(y).

%65 Any supermarket does not make item

all x all y SuperMarket(x) & -Make(x,y) & Item(y).

%66 Money can be in credit.

all x Money(x) & Money(credit) | Money(cash).

end_of_list.

formulas(goals).
%1. Is john a child or an adult ? [Adult]
%Adult(John).        

%2. Does John now have at least two tomatoes?[Yes] 
Owns(John,x,q2) & HasCount(x,c1)>=2.

%3. Did john buy any meat ?[Yes]
%exists y Buys(John,y,q,z) & Meat(y) & Quantity(q).        

%4. If Mary was buying tomatoes at the same time as John, did he see her?[Yes]
%At(John,Safeway) & At(Mary,Safeway) & SuperMarket(Safeway) & Tomato(p) & Buys(John,p,q,Safeway) & Buys(Mary,p,q,Safeway) & Sees(Mary,John). 
        
%5. Are the tomatoes made in the supermarket?[No]
%all x all y SuperMarket(x) & Tomato(y) & -Make(x,y).        

%6. What is John going to do with the tomatoes?[Eat them]
%all y Eats(John,y) & Tomato(y). 
        
%7. Does Safeway sell deodorant ?[yes] 
Deodorant(y) & Quantity(q) & Sells(Safeway,y,q).
       
%8.Did John bring some money or a credit card to the supermarket?[yes]
%all x all y all z all y At(John,z) & SuperMarket(z) ->  HasMoney(John,y)  & Money(y) & Money(credit) | Money(cash).
        
%9.Does John have less money after going to the supermarket? [Yes]
%HasMoney(John,m1) & Money(m1) & At(John,Safeway) -> HasMoney(John,m2) & Money(m2) & LessThan(m2,m1).

%10.Are there other people in Safeway while John is there? [Yes—staff!]        
At(x,Safeway) & Person(x) & At(John,Safeway).

%11.Is John a vegetarian? [No]        
%-Vegetarian(John).

%12.Who owns the deodorant in Safeway? [Safeway Corporation] or Does safeway owns deodorant ?
%Owns(Safeway,y,q) & Deodorant(y) &  Quantity(q).
        
%13.Did John have an ounce of ground beef? [yes] 
%Owns(John,ground_beef,o1) & Ounces(o1)=1.
       
%14.Does the Shell station next door have any gas?[Yes]  
%all x all y GasStation(Shell) & HasGas(Shell,y).
      

%15.Do the tomatoes fit in John's car trunk ?[yes]   
%Tomato(y2) &  HasCar(John,Camry) & HasTrunk(Camry,t1) & Car(Camry) & fit(t1,y2).
        

end_of_list.
