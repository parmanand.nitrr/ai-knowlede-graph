%1.All objects x that belong to the Tomato Class, is a part of the food class
fooditem(X) :- tomato(X).

%2.All object x that belong to the Meat Class, is a part of the food item class.
fooditem(X) :- meat(X).

%3. All items x belonging to the class Tomato also belong to the class
% veggie
veggie(X) :- tomato(X).

%4.All object x that belong to the Veggie Class, is a part of the Item
% class.
item(X) :- veggie(X).

%5. All objects that belong to the PersonalCare Class, belong to the
% Item class.
item(X) :- personal_care(X).

%6.All object x that belong to the Beef Class, is a part of the Meat
% class.
meat(X) :- beef(X).

%7.All objects that belong to the Deodorant Class, belong to the
% PersonalCare class.
personal_care(X) :- deodorant(X).

%8 All objects that belong to food class belong to item class
item(X):- fooditem(X).

%9 All staff are person.
person(X):- staff(X).

%10 All people who eat meat are non-vegetarians.
non_vegetarian(X) :- eats(X,Y) , fooditem(Y) , meat(Y).

%11 If supermarket owns something then they sell it.
sells(X,Y,Q) :- supermarket(X) , item(Y) , owns(X,Y,Q) , quantity(Q).

%12 If someone buys something and it is a food item, then they eat it.
eats(X,Y) :- buys(X,Y,Q,Z,D) , fooditem(Y),day(D).

%13. The location of Safeway is in NorthBerkeley as per the question
% statement.
location(safeway, north_berkeley).

%14 Every supermarket owns some quantity of some deodrant
supermarket(X) :- owns(X,Y,Q) , quantity(Q),  deodorant(Y).

%15 if X is above 18 then X is Adult.
adult(X):- person(X),age(X,Y),Y>= 18.

%16 X person buy Y item of Q quantity from Z supermarket.
owns(X,Y,Q):- buys(X,Y,Q,Z,D),item(Y),quantity(Q),day(D).

%17 all pounds are quantity.
quantity(X) :- pound(X).

%18 If supermarket owns something then they sell it.
sells(X,Y,Q) :- supermarket(X),item(Y),quantity(Y),owns(X,Y,Q).

%19 Any supermarket buys item from manufacturer
manufacturer(Z):-supermarket(X),buys(X,Y,Q,Z,D),quantity(Q),item(Y),day(D).

%20 X is located next to gas station Y.
next(X,Y) :- supermarket(X),gas_station(Y).

%21 Every gas station has gas
gas_station(X) :- has_gas(X,Q),quantity(Q).

%22 Any person X sees Y at supermarket Z and day D if X andY buys sa.
see(X,Y,Z,D):- buys(X,T,Q,Z,D),buys(Y,T,Q,Z,D),supermarket(Z),day(D).

%21 All Manufacturer make items
make(X,Y):- manufacturer(X),item(Y).

%22 All Supermarket does not make items.
make(X,Y):- not(supermarket(X)),item(Y).

%23 Any X is customer if X is at supermarket Z and buys items
customer(X) :- at(X,Z),supermarket(Z),buys(X,T,Q,Z,D),item(T),quantity(Q),day(D),person(X).

%24 if X is buying at supermarket then X has money.
has_money(X,M):- buys(X,T,Q,Z,D),supermarket(Z),money(M),item(T),quantity(Q),person(X),day(D),at(X,Z).

%24 X works at Y supermarket.
work(X,Y):- staff(X),supermarket(Y),at(X,Y).

%25 Any X is located at place Y
at(X,Y):- person(X),supermarket(Y).

%26 Convert  pound to ounce
convert_pound_to_ounce(Pound,Ounce):- Pound>0,Ounce is Pound*16.

%27  Y pound of tomato is equal to Number of items
convert_pound_to_numbers(Y,Pound,Number) :- tomato(Y),Pound>0,Number is Pound*4.

%28 Any X has car Y
has_car(X,Y) :- person(X),car(Y).

%29 Any Car X has Trunk T
has_trunk(X,T) :- car(X),trunk(T).

%30 Any  item Y can fit into car's trunk X.
fit(X,Y):- trunk(X),item(Y).

% 31 Any X person who buys item Y at supermarket Z has less money after
% buying
less_money(X,Y,Z) :- person(X),buys(X,Y,Q,Z,D),supermarket(Z),item(Y),quantity(Q).

%32 Yesterday john buys 2 pound of cherry tomatoes at safeway.
buys(john,cherry,2,safeway,yesterday).

%33 Yesterday John buys 1 pound of ground beef at safeway.
buys(john,ground_beef,1,safeway,yesterday).

%34 Yesterday Mary buys some pounds of cherry tomato at safeway.
buys(mary,cherry,Q,safeway,yesterday).

%35 Yesterday safeway buys 10 fog deodorant at tesco.
buys(safeway,fog,10,tesco,yesterday).

%36 John has a camery which is a car.
has_car(john,camry).

%37 Camry car has trunk.
has_trunk(camry,camry_s_trunk).

%38 camry_s_trunk is a trunk.
trunk(camry_s_trunck).

%39 camry is a car.
car(camry).

%40 John is at safeway.
at(john,safeway).

%41 Safeway is a supermarket
supermarket(safeway).

%42 yesterday is a type of day.
day(yesterday).

%43John is a person.
person(john).

%44 john is 21 year old
age(john,21).

%45 Mary is a person
person(mary).

%46 Mary is 20 year old.
age(mary,20).

%47  ground beef is a type of beef.
beef(ground_beef).

%48 cherry tomato is a type of tomato.
tomato(cherry).

%49 Fog is a type of deodorant.
deodorant(fog).

%50 tesco is a manufacturer.
manufacturer(tesco).

%51 sam is at safeway.
at(sam,safeway).

%52 sam is type of staff
staff(sam).

%53 safeway is located at North Berkeley.
at(safeway,north_berkeley).

%54 safeway is next to shell
next(safeway,shell).

%55 shell is a gas station.
gas_station(shell).

%56 shell gas station have 100 gallons of gas.
has_gas(shell,1000).

%57 North Berkeley is type of location.
location(north_berkeley).

%58 Money can be in form of cash.
money(cash).

%59 Money can be in the form of credit card.
money(credit_card).

%60 Any X brings money M at supermarket Y.
brings_money_at(X,M,Y):- has_money(X,M).

%61 Any Mobile X is type of item
item(X):-mobile(X).

%62 Any Book X is type of item
item(X) :- book(X).

%63 Any laptop is a product.
product(X):- laptop(X).

%64 asus 7 is a mobile.
mobile(asus_7).

%65 Any supermarket  has section L for item Y
has_section(X,Y,L) :- supermarket(X),item(Y),section(L).

%66 Any carrot is a type of product.
product(X):- carrot(X).

%67 Any Egg plant is a type of product.
product(X)  :- egg_plant(X).

%68 Any bread is a type of product.
product(X):-bread(X).

%69 Any Onion is a  type of product.
product(X) :- onion(X).


%70 Any grapes is a type of fruit.
fruit(X) :- grape(X).

%71 Any cookies is a type of snack.
snack(X) :- cookie(X).

%72 Any yogurt is a type of dairy.
dairy(X):- yogurt(X).

%73 Any Rice is type of grains

grain(X) :- rice(X).

%74 Any Peas is a type of product.
product(X) :- pea(X).


%75 basmati is type of rice.
rice(basmati).

%76 parle is a type of cookie.
cookie(parle).

%77 red onion is a type of onion.
onion(red_onion).

%78 red carror is a type of a carrot.
carrot(red_carrot).

%79 greek yogurt is a type of yogurt.
yogurt(greek_yogurt).

%80 Iphone 12 is a type of mobile.
mobile(i_phone_12).

%81 Zero to one is a type of book.

book(zero_to_one).

%82  green grape is a type of grape.
grape(green_grape).

%83 all orange is a type of fruit.
fruit(X) :- orange(X).

%84 green peas is a type of pea.
pea(green_peas).

%85 broccoli is a type of  product.
product(X) :- broccoli(X).

%87 Pineapple is a type of fruit.
fruit(X):-pineapple(X).

%88 almonds is a type of seeds.
seeds(X):-almonds(X).

%89 walnut is a type of seeds.
seeds(X):- walnut.

%90 pumpkin is a type of product.
product(X) :- pumpkin(X).

%91 corn is a type of grain.
grain(X) :- corn(X).

%92 millet is a type of grain.
grain(X) :- millet(X).

%93 rye is a type of grain
grain(X) :- rye(X).

%94 berry are a type of fruits.
fruit(X) :- berry(X).

%95 strawberry is a type of berry.
berry(strawberry).

%96 blueberry is a type of berry.
berry(blueberry).

%97 cranberry is a type of berry.
berry(cranberry).

%98 blue berry is a type of berry.
berry(blueberry).

%99 rasp berry is a type of berry.
berry(raspberry).

%100 All fruits are a type of product.
product(X) :- fruit(X).


pound(1).
pound(2).
pound(3).
pound(4).
pound(5).
quantity(1).
quantity(2).
quantity(3).
quantity(4).
quantity(5).
quantity(6).
quantity(7).
quantity(8).
quantity(9).
quantity(10).

























